from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
import json

if __name__ == "__main__":

    sc = SparkContext(appName="TweetCountHashTag")

    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, 120)
    #ssc.checkpoint("checkpoint_TwitterApp")

    dataStream = KafkaUtils.createStream(ssc, '10.106.219.194:2181', 'spark-streaming', {'tweets':1})

    full_tweet = dataStream.map(lambda v: json.loads(v[1]))
    words = full_tweet.flatMap(lambda tweet:tweet['text'].split(" "))

    hashtags = words.filter(lambda w: w =='#Colombia' or w =='#Venezuela')
    counts = hashtags.map(lambda word: (word, 1)).reduceByKey(lambda a, b: a + b)
#    counts = words.filter(lambda w: '#' in w).map(lambda x: (x, 1)).reduceByKey(lambda a, b: a + b)
    counts.pprint()

    ssc.start()
    ssc.awaitTermination()
