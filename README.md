# Plataforma Res-City

_En este repositorio se encuentran todos los pasos para instalar y correr el escenario de ejemplo_

![Escenario de prueba](Docs/Escenario.PNG)

## Pre-requisitos del sistema 📋

_A continuación se presentan los pre-requisitos del sistema:_

* [Virtual Box](https://www.virtualbox.org/wiki/Downloads) - Software de virtualización.
* [Vagrant](https://www.vagrantup.com/downloads.html) - Herramienta para la administración de máquinas virtuales.

## Instalación de los pre-requisitos del escenario 🔧

_Inicialmente, se debe instalar y configurar el entorno de vagrant que cuenta con 1 nodo maestro y 2 nodos auxiliares mediante los siguientes pasos:_

#### Instalar Entorno con vagrant

- Clone el siguiente repositorio mediante el uso de la instrucción:
```
git clone https://exxsyseng@bitbucket.org/exxsyseng/k8s_ubuntu.git
```

- Dentro de la carpeta del repositorio que contiene el Vagrantfile, inicie las máquinas virtuales: 
```
vagrant up
```

- Terminada la instalación, ingrese al nodo maestro:
```
vagrant ssh kmaster
```

- Verifique el funcionamiento listando los nodos del sistema:
```
kubectl get nodes
```

**Recuerde que los archivos mencionados en cada una de las guías se encuentran disponibles en este repositorio, si lo desea, clone el repositorio en su nodo maestro con el siguiente comando:**
```
git clone https://gitlab.com/hfmartinez/res-city.git
```

#### Instalar Kafka sobre kubernetes

_Para instalar Kafka sobre kubernetes siga la siguiente [guia](Docs/kafka/Guia_Kafka.pdf)._

#### Instalar Servidor de métricas y AutoScaling HPA

_Para instalar el servidor de métricas sobre kubernetes siga la siguiente [guia](Docs/metricas+hpa/GuiaServidorMetricas_HPA.pdf)._


#### Instalar Spark sobre kubernetes

_Para instalar el spark sobre kubernetes siga la siguiente [guia](Docs/spark/GuiaKubernetesSpark.pdf)._

**Nota: tenga presente que los nombres de los pods previamente instalados pueden variar, por lo tanto, valide los nombres con el comando** _kubectl get pods_


## Ejecutando las pruebas con el escenario ⚙️

#### Montaje final del escenario

_Para ejecutar el escenario es necesario instalar el pod encargado de la captura de tweets mediante la siguiente [guia](Docs/GetTweets/GuiaEscenario.pdf)._

**Nota: dentro de la carpeta de GetTweets se encuentran los scripts necesarios para ejecutar el programa, dependiendo de la IP asignada en la creación del broker de Kafka será necesario realizar modificaciones y volver a copiar estos archivos a sus respectivos Pods.** 

_Modifique el script [getTweets.py](Docs/GetTweets/getTweets.py) en la lÍnea 18, por la IP entregada por el servicio de Kafka, esta IP se puede validar con el siguiente comando:_

```
kubectl get services
```

_Modifique el script [count_hashtag.py](Docs/GetTweets/count_hashtag.py) en la lÍnea 14, por la IP entregada por el servicio de zookeeper, igualmente esta IP se puede validar con el siguiente comando:_

```
kubectl get services
```

_Una vez se modificaron los archivos mencionados con anterioridad, modifique el archivo que se encuentra en cada uno de los pods de la siguiente manera:_

```
##Para el archivo getTweets.py - modifique pod tweets-app
kubectl exec [Nombre de pod] rm /twitter/getTweets.py
kubectl cp getTweets.py [Nombre de pod]:/twitter

##Para el archivo count_hashtag.py - modifique pod tspark-master
kubectl exec [Nombre de pod] rm /opt/spark/count_hashtag.py
kubectl cp count_hashtag.py [Nombre de pod]:/opt/spark/
```
**Nota: cambie [Nombre de pod] por el nombre que se genera en su caso, este nombre se valida por medio de la instruccion** _kubectl get pods_ **, una vez realiza estas modificaciones, puede regresar a la [guia](Docs/GetTweets/GuiaEscenario.pdf), la cual contiene información necesaria para correr todo el entorno en conjunto.**
